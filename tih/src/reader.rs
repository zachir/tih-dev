use std::fs::File;
use std::io::prelude::*;
use serde_json::Value;
use crate::config::*;
use crate::Area;

pub fn read_file (filename: &str, room: &str, mut contents: String) -> std::io::Result<()> {
    let filepath: String = "../rooms/".to_owned() + room + "/" + filename + "." + LANG;
    let mut file = File::open(filepath.as_str())?;
    file.read_to_string(&mut contents)?;
    Ok(())
}

pub fn parse_area (contents: String) -> Area {
    let disc: bool;
    let mut next: Vec<u8> = Vec::new();
    let mut mess: Vec<String> = Vec::new();
    let mut prpt: Vec<String> = Vec::new();
    let debg: String;
    let area: Area;
    let v: Value = serde_json::from_str(&contents.as_str()).expect("Failed to read room file!");
    if v["disco"] != Value::Null && v["nexts"][0] != Value::Null && v["messg"][0] != Value::Null && v["prmpt"][0] != Value::Null && v["debug"] != Value::Null {
        disc = if v["disco"] == "true" { true } else { false};
        let nxtemp = v["nexts"].as_array().unwrap().len();
        for i in 0..nxtemp {
            next.push(v["nexts"][i].as_str().unwrap().parse().unwrap());
        }
        let mstemp = v["messg"].as_array().unwrap().len();
        for i in 0..mstemp {
            mess.push(v["messg"][i].to_string());
        }
        let pmtemp = v["prmpt"].as_array().unwrap().len();
        for i in 0..pmtemp {
            prpt.push(v["prmpt"][i].to_string());
        }
        debg = v["debug"].to_string();
        area = Area::new(disc, next, mess, prpt, debg);
    } else {
        disc = false;
        next.push(0);
        mess.push("Message".to_owned());
        prpt.push(" Prompt".to_owned());
        debg = "#Debug".to_owned();
        area = Area::new(disc, next, mess, prpt, debg);
    }
    area
}
