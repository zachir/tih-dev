extern crate serde_json;

pub mod reader;
pub mod config;

use std::fs::File;
use std::io::Result;
use std::io::prelude::*;

//pub struct Room {
//    d1: u32,
//    areas: Vec<Area>,
//    exitto: u8,
//}
//
pub struct Area {
    disco: bool,
    nexts: Vec<u8>,
    messg: Vec<String>,
    prmpt: Vec<String>,
    bmesg: String,
}

impl Area {
    pub fn new(d: bool, ns: Vec<u8>, ms: Vec<String>, pm: Vec<String>, db: String) -> Area {
        Area { disco: d, nexts: ns, messg: ms, prmpt: pm, bmesg: db }
    }
    pub fn to_string(&mut self) -> String {
        let mut temp = String::new();
        temp = temp + format!("disco: {}\n", self.disco).as_str();
        temp = temp + "nexts: \n";
        let nstemp = self.nexts.len();
        let mstemp = self.messg.len();
        let pmtemp = self.prmpt.len();
        for i in 0..nstemp {
            temp = temp + format!("\t{} {}\n", i+1, self.nexts[i]).as_str();
        }
        temp = temp + "messg: \n";
        for i in 0..mstemp {
            temp = temp + format!("\t{} {}\n", i+1, self.messg[i]).as_str();
        }
        temp = temp + "prmpt: \n";
        for i in 0..pmtemp {
            temp = temp + format!("\t{} {}\n", i+1, self.prmpt[i]).as_str();
        }
        temp = temp + format!("bmesg: {}\n", self.bmesg).as_str();
        temp
    }

    //disco
    pub fn set_disco(&mut self, disc: bool) {
        self.disco = disc;
    }

    //nexts
    pub fn set_nexts(&mut self, next: Vec<u8>) {
        self.nexts = next;
    }

    //messg
    pub fn set_messg(&mut self, mess: Vec<String>) {
        self.messg = mess;
    }

    //prmpt
    pub fn set_prmpt(&mut self, prpt: Vec<String>) {
        self.prmpt = prpt;
    }
}

pub fn check_files() -> bool {
    true
}

pub fn read_file(filename: &str, mut contents: &mut String) -> Result<()> {
    let mut file = File::open(filename)?;
    file.read_to_string(&mut contents)?;
    Ok(())
}
