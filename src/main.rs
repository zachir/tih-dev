extern crate tih;

use tih::reader::parse_area;
use tih::read_file;
fn main() {
    let mut jsonstring = String::new();
    read_file("/home/zachir/git/tih-dev/tih-rooms/room0/0.json", &mut jsonstring).expect("Error processing 0.json");
    let mut ar = parse_area(jsonstring);
    println!("{}\n", ar.to_string());
}
