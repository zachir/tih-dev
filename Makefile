LIBDIR=tih
BINSRC=src/main.rs
ROOMDIR=tih/rooms
LIBSRC=$(LIBDIR)/src/lib.rs

all: $(BINSRC) $(LIBSRC)
	cargo build --release

check: $(BINSRC) $(LIBSRC) checklib
	cargo check --release

checklib: $(LIBSRC)
	cd tih && cargo check --release

clean:
	-rm -rf target/ $(LIBDIR)/target
	-rm -f Cargo.lock $(LIBDIR)/Cargo.lock
